﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

软件工程专业2班  吕志文   202010414211

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

1.表设计方案：

创建主表空间用于存储主要的表和索引。

创建索引表空间用于存储索引数据。

创建临时表空间用于处理排序和临时表数据。



```
CREATE TABLESPACE users

 DATAFILE 'users.dbf'

 SIZE 100M

 AUTOEXTEND ON

 NEXT 100M;

CREATE TABLESPACE indexes

 DATAFILE 'indexes.dbf'

 SIZE 100M

 AUTOEXTEND ON

 NEXT 100M;

 

CREATE TABLE products (

 product_id  NUMBER PRIMARY KEY,

 product_name VARCHAR2(100),

 price     NUMBER,

 stock     NUMBER

);

 

CREATE TABLE customers (

 customer_id NUMBER PRIMARY KEY,

 name     VARCHAR2(100),

 address   VARCHAR2(200),

 phone    VARCHAR2(20)

);

 

CREATE TABLE orders (

 order_id   NUMBER PRIMARY KEY,

 customer_id NUMBER,

 product_id  NUMBER,

 quantity   NUMBER,

 order_date  DATE,

 CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id) REFERENCES customers(customer_id),

 CONSTRAINT fk_orders_products FOREIGN KEY (product_id) REFERENCES products(product_id)

);

 

CREATE TABLE sales (

 sale_id    NUMBER PRIMARY KEY,

 order_id   NUMBER,

 sale_date   DATE,

 amount    NUMBER,

 sales_person VARCHAR2(100),

 CONSTRAINT fk_sales_orders FOREIGN KEY (order_id) REFERENCES orders(order_id)

);
```

![img](file:///C:\Users\awen\AppData\Local\Temp\ksohtml14776\wps1.jpg) 

![img](file:///C:\Users\awen\AppData\Local\Temp\ksohtml14776\wps2.jpg) 

2. 模拟数据

-- 向商品表插入模拟数据

```
DECLARE

 i NUMBER;

BEGIN

 FOR i IN 1..100000 LOOP

  INSERT INTO products (product_id, name, description, price)

  VALUES (i, 'Product ' || i, 'Description ' || i, i * 10);

 END LOOP;

 COMMIT;

END;

/

 

-- 向客户表插入模拟数据

DECLARE

 i NUMBER;

BEGIN

 FOR i IN 1..50000 LOOP

  INSERT INTO customers (customer_id, name, address, contact)

  VALUES (i, 'Customer ' || i, 'Address ' || i, 'Contact ' || i);

 END LOOP;

 COMMIT;

END;

/

 

-- 向订单表插入模拟数据

DECLARE

 i NUMBER;

BEGIN

 FOR i IN 1..20000 LOOP

  INSERT INTO orders (order_id, customer_id, order_date)

  VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 50000)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 100)));

 END LOOP;

 COMMIT;

END;

/

 

-- 向订单明细表插入模拟数据

DECLARE

 i NUMBER;

BEGIN

 FOR i IN 1..300000 LOOP

  INSERT INTO order_details (order_id, product_id, quantity)

  VALUES (ROUND(DBMS_RANDOM.VALUE(1, 20000)), ROUND(DBMS_RANDOM.VALUE(1, 100000)), ROUND(DBMS_RANDOM.VALUE(1, 10)));

 END LOOP;

 COMMIT;

END;

/
```

 

3.用户和权限分配方案：

创建销售用户：用于管理商品表、客户表和订单表的权限。

创建查询用户：用于只读访问商品表、客户表和订单表的权限。

```
CREATE USER admin IDENTIFIED BY admin_password DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

GRANT CONNECT, RESOURCE, CREATE SESSION, CREATE VIEW TO admin;

CREATE USER regular_user IDENTIFIED BY user_password DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

GRANT CONNECT, RESOURCE, CREATE SESSION TO regular_user;
```

![img](file:///C:\Users\awen\AppData\Local\Temp\ksohtml14776\wps3.jpg) 

 

 

 

 

 

 

 

4.程序包和存储过程设计：

创建一个程序包用于封装业务逻辑。

在程序包中设计存储过程和函数，实现以下功能：

创建订单：接收客户ID和商品明细，生成订单并更新订单表和订单明细表。

查询订单总额：根据订单ID计算订单的总金额。

更新商品价格：根据商品ID更新商品表中的价格。

根据客户ID查询订单：返回指定客户的所有订单信息。

```
CREATE OR REPLACE PACKAGE sales_pkg AS

 PROCEDURE create_order(

  p_customer_id IN NUMBER,

  p_product_id  IN NUMBER,

  p_quantity   IN NUMBER

 );

 

 PROCEDURE calculate_sales(

  p_start_date  IN DATE,

  p_end_date   IN DATE,

  p_total_sales OUT NUMBER,

  p_avg_sales  OUT NUMBER

 );

 

 FUNCTION get_customer_order_count(

  p_customer_id IN NUMBER

 ) RETURN NUMBER;

END sales_pkg;

 

CREATE OR REPLACE PACKAGE BODY sales_pkg AS

 PROCEDURE create_order(

  p_customer_id IN NUMBER,

  p_product_id  IN NUMBER,

  p_quantity   IN NUMBER

 ) IS

  v_stock products.stock%TYPE;

 BEGIN
```

![img](file:///C:\Users\awen\AppData\Local\Temp\ksohtml14776\wps4.jpg) 

 

 

5.数据库备份方案：

定期进行全量备份，包括数据文件、控制文件和归档日志。

使用Oracle提供的RMAN工具进行备份和恢复操作。

将备份文件存储在独立的存储设备上，以确保数据的可靠性和安全性。

 

```
 SELECT stock INTO v_stock FROM products WHERE product_id = p_product_id;

  

  IF v_stock >= p_quantity THEN

   INSERT INTO orders (order_id, customer_id, product_id, quantity, order_date)

   VALUES (order_seq.NEXTVAL, p_customer_id, p_product_id, p_quantity, SYSDATE);

   

   UPDATE products SET stock = stock - p_quantity WHERE product_id = p_product_id;

   

   COMMIT;

  ELSE

   RAISE_APPLICATION_ERROR(-20001, 'Insufficient stock for the product');

  END IF;

 END create_order;

 

 PROCEDURE calculate_sales(

  p_start_date  IN DATE,

  p_end_date   IN DATE,

  p_total_sales OUT NUMBER,

  p_avg_sales  OUT NUMBER

 ) IS

 BEGIN

SELECT SUM(amount) INTO p_total_sales FROM sales WHERE sale_date BETWEEN p_start_date AND p_end_date;
```

![img](file:///C:\Users\awen\AppData\Local\Temp\ksohtml14776\wps5.jpg)
