-- 创建表空间
CREATE TABLESPACE users
  DATAFILE 'users.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M;

CREATE TABLESPACE indexes
  DATAFILE 'indexes.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M;

-- 创建表
CREATE TABLE products (
  product_id   NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  price        NUMBER,
  stock        NUMBER
);

CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  name        VARCHAR2(100),
  address     VARCHAR2(200),
  phone       VARCHAR2(20)
);

CREATE TABLE orders (
  order_id    NUMBER PRIMARY KEY,
  customer_id NUMBER,
  product_id  NUMBER,
  quantity    NUMBER,
  order_date  DATE,
  CONSTRAINT fk_orders_customers FOREIGN KEY (customer_id) REFERENCES customers(customer_id),
  CONSTRAINT fk_orders_products FOREIGN KEY (product_id) REFERENCES products(product_id)
);

CREATE TABLE sales (
  sale_id      NUMBER PRIMARY KEY,
  order_id     NUMBER,
  sale_date    DATE,
  amount       NUMBER,
  sales_person VARCHAR2(100),
  CONSTRAINT fk_sales_orders FOREIGN KEY (order_id) REFERENCES orders(order_id)
);
-- 向商品表插入模拟数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO products (product_id, name, description, price)
    VALUES (i, 'Product ' || i, 'Description ' || i, i * 10);
  END LOOP;
  COMMIT;
END;
/

-- 向客户表插入模拟数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO customers (customer_id, name, address, contact)
    VALUES (i, 'Customer ' || i, 'Address ' || i, 'Contact ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 向订单表插入模拟数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..20000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 50000)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 100)));
  END LOOP;
  COMMIT;
END;
/

-- 向订单明细表插入模拟数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..300000 LOOP
    INSERT INTO order_details (order_id, product_id, quantity)
    VALUES (ROUND(DBMS_RANDOM.VALUE(1, 20000)), ROUND(DBMS_RANDOM.VALUE(1, 100000)), ROUND(DBMS_RANDOM.VALUE(1, 10)));
  END LOOP;
  COMMIT;
END;
/

-- 创建用户和授权
CREATE USER admin IDENTIFIED BY admin_password DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
GRANT CONNECT, RESOURCE, CREATE SESSION, CREATE VIEW TO admin;

CREATE USER regular_user IDENTIFIED BY user_password DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
GRANT CONNECT, RESOURCE, CREATE SESSION TO regular_user;

-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS
  PROCEDURE create_order(
    p_customer_id IN NUMBER,
    p_product_id  IN NUMBER,
    p_quantity    IN NUMBER
  );
  
  PROCEDURE calculate_sales(
    p_start_date  IN DATE,
    p_end_date    IN DATE,
    p_total_sales OUT NUMBER,
    p_avg_sales   OUT NUMBER
  );
  
  FUNCTION get_customer_order_count(
    p_customer_id IN NUMBER
  ) RETURN NUMBER;
END sales_pkg;

CREATE OR REPLACE PACKAGE BODY sales_pkg AS
  PROCEDURE create_order(
    p_customer_id IN NUMBER,
    p_product_id  IN NUMBER,
    p_quantity    IN NUMBER
  ) IS
    v_stock products.stock%TYPE;
  BEGIN
    -- 获取产品库存
    SELECT stock INTO v_stock FROM products WHERE product_id = p_product_id;
    
    -- 检查库存是否足够
    IF v_stock >= p_quantity THEN
      -- 创建订单
      INSERT INTO orders (order_id, customer_id, product_id, quantity, order_date)
      VALUES (order_seq.NEXTVAL, p_customer_id, p_product_id, p_quantity, SYSDATE);
      
      -- 更新产品库存
      UPDATE products SET stock = stock - p_quantity WHERE product_id = p_product_id;
      
      COMMIT;
    ELSE
      RAISE_APPLICATION_ERROR(-20001, 'Insufficient stock for the product');
    END IF;
  END create_order;
  
  PROCEDURE calculate_sales(
    p_start_date  IN DATE,
    p_end_date    IN DATE,
    p_total_sales OUT NUMBER,
    p_avg_sales   OUT NUMBER
  ) IS
  BEGIN
    -- 计算销售总额
    SELECT SUM(amount) INTO p_total_sales FROM sales WHERE sale_date BETWEEN p_start_date AND p_end_date;
    
    -- 计算
